const express = require ('express');
const morgan =  require ('morgan');
const exphbs = require ('express-handlebars');
const path = require('path');

//Initializations
const app = express();

//Settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main', // Predeterminated file.
    layoutsDir: path.join(app.get('views'), 'layouts'), //Unir directorios.
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}))
app.set('view engine', '.hbs');

// Middleware (peticiones por función)
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
//Recevied and send json
app.use(express.json());

//Global variables
app.use((req, res, next) => {
    next();
});

// Routes
app.use(require('./routes'));
app.use(require('./routes/authentication'));
app.use('/nuxiba', require('./routes/login'));
app.use('/nuxiba', require('./routes/vacations'));
app.use('/nuxiba', require('./routes/menu'));
app.use('/nuxiba', require('./routes/permissions'));
app.use('/nuxiba', require('./routes/resumeVacationsRequest'));
app.use('/nuxiba', require('./routes/resumePermissionRequest'));

// Public files
app.use(express.static(path.join(__dirname, 'public')));

//Starting the server
app.listen(app.get('port'), () => {
    console.log('Server on port: ', app.get('port'));
});
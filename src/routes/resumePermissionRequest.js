const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/resumePermissionRequest', (req, res) => {
    res.render('permissions/resumePermissionRequest')
});

module.exports = router;
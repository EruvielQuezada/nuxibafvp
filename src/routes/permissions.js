const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/generatePermission', (req, res) => {
    res.render('permissions/generatePermission')
});

module.exports = router;
const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/generateVacations', (req, res) => {
    res.render('vacations/generateVacations')
});

router.post('/generateVacations', async (req, res) => {
    const { requestDays, vacationPeriod, dateRange } = req.body;
    const newVacationRequest = {
        requestDays,
        vacationPeriod,
        dateRange
    };
    await pool.query('INSERT INTO users SET ?', [newVacationRequest]);
    res.send('RECEIVED');
});

module.exports = router;
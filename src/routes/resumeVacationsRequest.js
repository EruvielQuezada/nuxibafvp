const express = require('express');
const router = express.Router();

const pool = require('../database');

router.get('/resumeVacationsRequest', (req, res) => {
    res.render('vacations/resumeVacationsRequest')
});

module.exports = router;
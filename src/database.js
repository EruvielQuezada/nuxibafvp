const mysql = require('mysql');
const {promisify} = require('util');
const { database } = require('./keys');

// Threads to sequence.
const pool = mysql.createPool(database);
// Callback.
pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('DATABASE CONNECTION WAS INTERRUPTED.')
        }
        if (err.code === 'ER__CON_COUNT_ERROR') {
            console.error('DATABASE HAS TO MANY CONNECTIONS.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('DATABASE CONNECTION WAS REFUSED.')
        }
    }
    if (connection) connection.release();
    console.log('DATABASE CONNECTED.');
    return;
});

// Promisify Pool Query.
pool.query = promisify(pool.query);
module.exports = pool;
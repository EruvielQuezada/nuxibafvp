CREATE DATABASE nuxibafvp_db;
USE nuxibafvp_db;

CREATE TABLE users(
    userID INT (11) NOT NULL,
    lastName VARCHAR (50) NOT NULL,
    mothersName VARCHAR (50) NOT NULL,
    fullName VARCHAR (60) NOT NULL,
    email VARCHAR (30) NOT NULL,
    password VARCHAR (8) NOT NULL,
    areaID INT (11),
    positionID INT (11),
    admissionDate DATE,
    userStatus BOOLEAN NOT NULL,
    CONSTRAINT fk_area FOREIGN KEY (areaID) REFERENCES areas(areaID),
    CONSTRAINT fk_position FOREIGN KEY (positionID) REFERENCES positions(positionsID)
);

ALTER TABLE users ADD PRIMARY KEY (userID);
ALTER TABLE users MODIFY userID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE boss (
    bossID INT (11),
    userID INT (11),
    CONSTRAINT fk_boss FOREIGN KEY (userID) REFERENCES users(userID)
);

ALTER TABLE boss ADD PRIMARY KEY (bossID);
ALTER TABLE boss MODIFY bossID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE positions (
    positionID INT (11),
    name VARCHAR (60)
);

ALTER TABLE positions ADD PRIMARY KEY (positionID);
ALTER TABLE positions MODIFY positionID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE permissionsRequest (
    permissionsRequestID INT (11),
    permissionID INT (11),
    userID INT (11),
    permissionRequestCreationDate timestamp NOT NULL DEFAULT current_timestamp,
    dateOfAbsenceOrDelay DATE,
    delayOrEarlyDepartureTime DATE,
    permissionDays INT (3),
    laborReturnByAbsence DATE,
    salaryStatus BOOLEAN,
    requestPermissionStatus BOOLEAN,
    motiveID INT (11),
    CONSTRAINT fk_user FOREIGN KEY (userID) REFERENCES users(userID)
    CONSTRAINT fk_motive FOREIGN KEY (motiveID) REFERENCES motives(motiveID),
    CONSTRAINT fk_permission FOREIGN KEY (permissionID) REFERENCES permissions (permissionID),
    
);

ALTER TABLE permissionsRequest ADD PRIMARY KEY (permissionsRequestID);
ALTER TABLE permissionsRequest MODIFY permissionsRequestID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE vacationsRequest (
    vacationRequestID INT (11),
    userID INT (11),
    vacationRequestCreationDate timestamp NOT NULL DEFAULT current_timestamp,
    availableDays INT (3),
    daysUsed INT (3),
    remainingDays INT (3),
    vacationStartPeriod DATE,
    vacationEndPeriod DATE,
    vacationRequestStatus BOOLEAN,
    CONSTRAINT fk_userID FOREIGN KEY (userID) REFERENCES users(userID)
);

ALTER TABLE vacationsRequest ADD PRIMARY KEY (vacationsRequestID);
ALTER TABLE vacationsRequest MODIFY vacationsRequestID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE permissions (
    permissionID INT (11),
    name VARCHAR (60)
);

ALTER TABLE permissions ADD PRIMARY KEY (permissionID);
ALTER TABLE permissions MODIFY permissionID INT (11) NOT NULL AUTO_INCREMENT;

CREATE TABLE motives (
    motiveID INT (11),
    name VARCHAR (60),
    description TEXT
);

ALTER TABLE motives ADD PRIMARY KEY (motiveID);
ALTER TABLE motives MODIFY motiveID INT (11) NOT NULL AUTO_INCREMENT;